package webjust.org;

public class CharDemo {
    public static void main(String[] args) {
        // 定义一个字符变量存放字符'a'
        char a = 'a';
        char ch = 65;
        char ch2 = 65535;
        // 如果字面值超出了char类型所表示的数据范围，需要进行强制数据类型转换
        char ch3 = (char) 65536;
        System.out.println(a);  // a
        System.out.println(ch); // A
        System.out.println(ch2);
        System.out.println(ch3);
        char c = '\u005d';
        System.out.println("c=" + c);   // c=]
    }
}