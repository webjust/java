package webjust.org;

public class FinalDemo {
    public static void main(String[] args) {
        int m = 10;
        final int n = 1;
        m = 20;
//        n = 2;  //语法错误
        final double PI = 3.1415926;
        final double MIN_VALUE = 0;
    }
}
