package webjust.org;

public class TypeExchange {
    public static void main(String[] args) {
        // char类型和int类型之间的转换
        char c = (char) 65535;
        int n;
        n = c;  // 隐式类型转换
        System.out.println(c);
        System.out.println(n);

        // 整型和浮点型类型之间的转换
        int x = 100;
        long y = x;
        x = (int) y;
        float f = 100000000000000L;
        System.out.println("f=" + f);   //f=1.0E14
        // 自动类型转换出现数据丢失问题
        float f1 = 134238877665544L;
        System.out.println("f1=" + f1); // f1=1.34238876E14
    }
}
